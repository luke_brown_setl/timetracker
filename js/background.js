/* globals chrome, URL */
/**
 * background.js is always running with Chrome is the extension is enabled.
 * The extension uses this feature to update the extension icon and to display
 * a warning if the user has loaded a production socket server (if log setting is enabled).
 */
