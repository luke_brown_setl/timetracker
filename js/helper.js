/* globals highchart */

function getDateOfWeek(w, y) {
    "use strict";

    var d = (1 + (w - 1) * 7) + 1; // 1st of January + 7 days for each week

    return {
        mon: new Date(y, 0, d),
        fri: new Date(y, 0, (d + 4))
    };
}

function addOrd(n) {
    "use strict";

    var ordinals = ['', 'st', 'nd', 'rd'];
    var m = n % 100;
    return n + ((m > 10 && m < 14) ? 'th' : ordinals[m % 10] || 'th');
}

function getMinutesBetweenDates(startDate, endDate) {
    "use strict";

    var diff = endDate.getTime() - startDate.getTime();
    return (diff / 60000);
}

function getWeekNumber(d) {
    "use strict";

    // Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0, 0, 0);
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    // Get first day of year
    var yearStart = new Date(d.getFullYear(), 0, 1);
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1) / 7);
    // Return array of year and week number
    return [d.getFullYear(), weekNo];
}


/** Minutes to Time.
 *
 * Converts a large number of minutes into the number of respective hours and left over minutes.
 *
 * @param {int} {amountOfMinutes} - A total number of minutes.
 *
 * @return string - A total number of minutes.
 */
function formatMinutes(amountOfMinutes) {
    "use strict";

    var minutes = parseInt(amountOfMinutes);

    // if no data is passed
    if (isNaN(amountOfMinutes)) {
        return "00:00";
    }

    // Workout hours, then work out how much of an hour is left over.
    var hours = Math.floor(minutes / 60);

    // Then workout the number of minutes remaining.
    minutes = minutes % 60;

    // Add a leading zero if hour or minutes are below 10.
    minutes = minutes < 10 ? "0" + minutes : minutes.toString();
    hours = hours < 10 ? "0" + hours : hours.toString();

    return hours + ':' + minutes;
}


function formattedTimeDifferent(startTime, endTime, type) {
    "use strict";

    var diff = 0;
    if (type === 'total') {
        diff = startTime - endTime;
    } else {
        diff = getMinutesBetweenDates(new Date(Date.UTC(0, 0, 0, startTime[0], startTime[1], 0)), new Date(Date.UTC(0, 0, 0, endTime[0], endTime[1], 0)));
    }

    if (isNaN(diff)) {
        diff = 0;
    }
    return {
        minutes: diff,
        formattedTime: formatMinutes(diff)
    };
}

function loadPreviousWeek() {
    "use strict";

    if (localStorage.selectedWeek === "1") {
        localStorage.selectedWeek = "52";
        localStorage.selectedYear--;
    } else {
        localStorage.selectedWeek--;
    }
    highchart(localStorage.selectedWeek);
}

function loadNextWeek() {
    "use strict";

    if (localStorage.selectedWeek === "52") {
        localStorage.selectedWeek = "1";
        localStorage.selectedYear++;
    } else {
        localStorage.selectedWeek++;
    }
    highchart(localStorage.selectedWeek);
}