/* global Highcharts, getDateOfWeek, addOrd, getWeeklyData*/
function highchart(currentWeek) {
    "use strict";


    $('#weekNumber').html('Week ' + localStorage.selectedWeek);

    var outputMon = getDateOfWeek(localStorage.selectedWeek, localStorage.selectedYear).mon;
    var outputFri = getDateOfWeek(localStorage.selectedWeek, localStorage.selectedYear).fri;

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];


    var dateMon = outputMon.getDate();
    var monthMon = outputMon.getMonth();
    var dateFri = outputFri.getDate();
    var monthFri = outputFri.getMonth();

    Highcharts.chart('box', {

        chart: {
            type: 'columnrange',
            inverted: true
        },

        title: {
            text: addOrd(dateMon) + ' ' + monthNames[monthMon] + ' - ' + addOrd(dateFri) + ' ' + monthNames[monthFri]
        },

        credits: {
            enabled: false
        },

        xAxis: {
            categories: ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri']
        },


        yAxis: {
            type: 'datetime',
            title: {
                text: 'Hours'
            }
        },

        plotOptions: {
            columnrange: {
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        var d = new Date(this.y);

                        var minutes = d.getMinutes();
                        if (d.getMinutes() < 10) {
                            minutes = '0' + d.getMinutes();
                        }
                        return d.getHours() + ':' + minutes;
                    }
                }
            }
        },

        legend: {
            enabled: false
        },

        tooltip: {
            formatter: function () {
                return formatAMPM(this);
            }
        },

        //colors: ['#7cb5ec', '#434348'],

        series: [{
            name: 'Working Hours',
            data: getWeeklyData(currentWeek),
            animation: {
                duration: 150
            }
        }]

    });
}

function formatAMPM(date) {
    "use strict";

    var date1 = new Date(date.point.low);
    var date2 = new Date(date.point.high);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = timeDiff / (1000 ) / 3600;

    var key = 'Working Day without Lunch';
    if (date.point.key === 1) {
        key = 'Lunch';
    }

    var hoursAndMinutes = diffDays.toString().split('.');
    var hours = hoursAndMinutes[0];
    var minutes = hoursAndMinutes[1].substring(0, 2);
    minutes = Math.ceil(((minutes / 100) * 60));

    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    return key + ' ' + hours + ':' + minutes;
}