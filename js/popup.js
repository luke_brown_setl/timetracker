/* globals chrome, getDateOfWeek, addOrd, getMinutesBetweenDates, getWeekNumber, formattedTimeDifferent, formatMinutes, loadPreviousWeek, loadNextWeek */
/**
 * popup.js runs when the extension is opened by the user.
 * It handles all of the settings and saving
 */

if (chrome.tabs !== undefined) {
    chrome.tabs.getSelected(null, function (tab) {
        "use strict";
        var tabLink = tab.url;
        $("#status").html(tabLink);
    });
}

$(document).ready(function () {
    "use strict";

    hardcodedData();

    getYearlyData();

    console.log(JSON.parse(localStorage.weeks));


    var currentWeek = new Date();
    localStorage.currentYear = getWeekNumber(currentWeek)[0];
    localStorage.currentWeek = getWeekNumber(currentWeek)[1];

    // set selected week as current week
    localStorage.selectedYear = localStorage.currentYear;
    localStorage.selectedWeek = localStorage.currentWeek;

    highchart(localStorage.currentWeek);


    $("body").keydown(function (e) {
        if (e.which === 37) {
            loadPreviousWeek();
        } else if (e.which === 39) {
            loadNextWeek();
        }
    });

    $("#left").on('click', function () {
        loadPreviousWeek();
    });

    $("#right").on('click', function () {
        loadNextWeek();
    });


    var edit = "#edit";
    $(edit).on('click', function () {

        if ($(edit).hasClass('active')) {
            $(edit).removeClass('active');
            highchart(localStorage.selectedWeek);
            $("#editForm").fadeOut();
            $("#box").delay(400).fadeIn();
        } else {
            $(edit).addClass('active');
            $("#box").fadeOut();
            $("#editForm").delay(400).fadeIn();
        }
    });

});


function getWeeklyData(currentWeek) {
    "use strict";

    var data = JSON.parse(localStorage.weeks)[currentWeek];
    var formattedData = [];

    var dayTotal = 0;
    var lunchTotal = 0;
    var totalTotal = 0;

    //console.warn('-------');
    $.each(data, function (index, value) {

        var startTime = value.startTime.split('.');
        var endTime = value.endTime.split('.');
        var lunchStartTime = value.lunchStartTime.split('.');
        var lunchEndTime = value.lunchEndTime.split('.');
        formattedData.push({
            x: index,
            key: 0,
            low: Date.UTC(0, 0, 0, startTime[0], startTime[1], 0),
            high: Date.UTC(0, 0, 0, endTime[0], endTime[1], 0)
        }, {
            x: index,
            key: 1,
            low: Date.UTC(0, 0, 0, lunchStartTime[0], lunchStartTime[1], 0),
            high: Date.UTC(0, 0, 0, lunchEndTime[0], lunchEndTime[1], 0),
            color: "#2f4050"
        });
        //console.log(Date.UTC(0, 0, 0, lunchStartTime[0], lunchStartTime[1], 0));

        var workingTime = formattedTimeDifferent(startTime, endTime);
        var lunchTime = formattedTimeDifferent(lunchStartTime, lunchEndTime);
        var actualWorkingTime = formattedTimeDifferent(workingTime.minutes, lunchTime.minutes, 'total');
        //var minutes = formatMinutes(diff2);

        //console.log('workingTime', workingTime);
        //console.log('lunchTime', lunchTime);
        //console.log('actualWorkingTime', actualWorkingTime);


        $('#day' + index).html(workingTime.formattedTime);
        $('#lunch' + index).html(lunchTime.formattedTime);
        $('#total' + index).html(actualWorkingTime.formattedTime);
        //console.log(diff / 60, startTime, endTime);

        dayTotal += workingTime.minutes;
        lunchTotal += lunchTime.minutes;
        totalTotal += actualWorkingTime.minutes;

    });

    $('#dayTotal').html(formatMinutes(dayTotal));
    $('#lunchTotal').html(formatMinutes(lunchTotal));
    $('#totalTotal').html(formatMinutes(totalTotal));


    return formattedData;
}


function getYearlyData() {
    "use strict";


    var data = JSON.parse(localStorage.weeks);
    //console.log(localStorage.weeks);
    var formattedData = [];

    var dayTotal = 0;
    var lunchTotal = 0;
    var totalTotal = 0;

    //console.warn('-------');
    $.each(data, function (index2, value2) {

        var weekOfData = data[index2];

        if (weekOfData !== null) {
            $.each(weekOfData, function (index, value) {


                if (value !== null) {
                    var startTime = value.startTime.split('.');
                    var endTime = value.endTime.split('.');
                    var lunchStartTime = value.lunchStartTime.split('.');
                    var lunchEndTime = value.lunchEndTime.split('.');

                    var workingTime = formattedTimeDifferent(startTime, endTime);
                    var lunchTime = formattedTimeDifferent(lunchStartTime, lunchEndTime);
                    var actualWorkingTime = formattedTimeDifferent(workingTime.minutes, lunchTime.minutes, 'total');

                    dayTotal += workingTime.minutes;
                    lunchTotal += lunchTime.minutes;
                    totalTotal += actualWorkingTime.minutes;
                }
            });
        }
    });

    $('#year').html(Math.round(totalTotal / 60));
    $('#yearLunch').html(Math.round(lunchTotal / 60));
}

function hardcodedData() {
    "use strict";

    var data = [{
        startTime: "9.00",
        endTime: "20.33",
        lunchStartTime: "12.03",
        lunchEndTime: "13.06"
    }, {
        startTime: "9.10",
        endTime: "18.27",
        lunchStartTime: "12.17",
        lunchEndTime: "12.36"
    }, {
        startTime: "9.17",
        endTime: "17.35",
        lunchStartTime: "12.40",
        lunchEndTime: "12.54"
    }, {
        startTime: "9.18",
        endTime: "17.26",
        lunchStartTime: "12.39",
        lunchEndTime: "14.04"
    }, {
        startTime: "9.07",
        endTime: "17.30",
        lunchStartTime: "12.07",
        lunchEndTime: "12.46"
    }];
    var yearOfData = [];

    yearOfData[26] = data;


    var data2 = [{
        startTime: "9.00",
        endTime: "17.50",
        lunchStartTime: "12.49",
        lunchEndTime: "13.30"
    }, {
        startTime: "8.52",
        endTime: "17.57",
        lunchStartTime: "12.47",
        lunchEndTime: "13.20"
    }, {
        startTime: "8.48",
        endTime: "17.53",
        lunchStartTime: "12.20",
        lunchEndTime: "13.25"
    }, {
        startTime: "9.04",
        endTime: "17.35",
        lunchStartTime: "12.46",
        lunchEndTime: "13.29"
    }, {
        startTime: "8.52",
        endTime: "17.34",
        lunchStartTime: "12.25",
        lunchEndTime: "12.49"
    }];

    yearOfData[25] = data2;
    yearOfData[27] = data2;


    var dataY = [{
        startTime: "9.05",
        endTime: "17.50",
        lunchStartTime: "12:50",
        lunchEndTime: "13:26"
    }, {
        startTime: "",
        endTime: "",
        lunchStartTime: "",
        lunchEndTime: ""
    }, {
        startTime: "",
        endTime: "",
        lunchStartTime: "",
        lunchEndTime: ""
    }, {
        startTime: "",
        endTime: "",
        lunchStartTime: "",
        lunchEndTime: ""
    }, {
        startTime: "",
        endTime: "",
        lunchStartTime: "",
        lunchEndTime: ""
    }];

    yearOfData[27] = dataY;

    localStorage.weeks = JSON.stringify(yearOfData);
}